package info.earty.workingcard.infrastructure.jms.dto;

import lombok.Data;

import java.time.Instant;
import java.util.List;
import java.util.Set;

@Data
public class CardDraftDiscardedJsonDto {

    private int id;
    String workingCardId;
    
    List<String> priorDraftImages;
    List<String> currentImages;
    Set<String> draftImagesRemoved;

    List<String> priorDraftAttachments;
    List<String> currentAttachments;
    Set<String> draftAttachmentsRemoved;
    
    Instant occurredOn;

}
