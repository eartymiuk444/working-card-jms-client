package info.earty.workingcard.infrastructure.jms.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class ImageRemovedJsonDto {

    private int id;
    String workingCardId;
    String imageRemoved;
    boolean imageOrphaned;
    Instant occurredOn;

}
