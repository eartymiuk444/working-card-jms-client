package info.earty.workingcard.infrastructure.jms.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class AddAttachmentFailedJsonDto {

    private int id;
    String workingCardId;
    String attachmentId;
    Instant occurredOn;

}
