package info.earty.workingcard.infrastructure.jms.dto;

import lombok.Data;

import java.time.Instant;
import java.util.List;
import java.util.Set;

@Data
public class CardPublishedJsonDto {

    private int id;
    String workingCardId;
    
    List<String> priorPublishedImages;
    List<String> currentPublishedImages;
    Set<String> publishedImagesRemoved;
    Set<String> publishedImagesAdded;

    List<String> priorPublishedAttachments;
    List<String> currentPublishedAttachments;
    Set<String> publishedAttachmentsRemoved;
    Set<String> publishedAttachmentsAdded;
    
    Instant occurredOn;

}
