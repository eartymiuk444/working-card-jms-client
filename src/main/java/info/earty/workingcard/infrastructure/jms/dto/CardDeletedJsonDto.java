package info.earty.workingcard.infrastructure.jms.dto;

import lombok.Data;

import java.time.Instant;
import java.util.List;
import java.util.Set;

@Data
public class CardDeletedJsonDto {

    private int id;
    String workingCardId;
    
    List<String> publishedImages;
    List<String> draftImages;
    Set<String> orphanedImages;

    List<String> publishedAttachments;
    List<String> draftAttachments;
    Set<String> orphanedAttachments;
    
    Instant occurredOn;

}
