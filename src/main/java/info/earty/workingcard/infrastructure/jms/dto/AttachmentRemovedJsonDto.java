package info.earty.workingcard.infrastructure.jms.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class AttachmentRemovedJsonDto {

    private int id;
    String workingCardId;
    String attachmentRemoved;
    boolean attachmentOrphaned;
    Instant occurredOn;

}
