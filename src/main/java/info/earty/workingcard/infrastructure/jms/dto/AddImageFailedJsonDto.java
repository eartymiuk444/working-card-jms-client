package info.earty.workingcard.infrastructure.jms.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class AddImageFailedJsonDto {

    private int id;
    String workingCardId;
    String imageId;
    Instant occurredOn;

}
